// Get the modal
const modal = document.querySelector(".modal");

// Get the button that opens the modal
const btn = document.getElementsByClassName("open_btn");

// Get the <span> element that closes the modal
const close_btn = document.querySelector(".close_btn");

// When the user clicks the button, open the modal

btn[0].addEventListener("click", () => {
    modal.style.display = "block";
});

close_btn.addEventListener("click", () => {
    modal.style.display = "none";
});
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
};